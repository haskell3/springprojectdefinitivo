package HaskellDefinitivisimo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"HaskellDefinitivisimo"})
@EntityScan("HaskellDefinitivisimo")
@EnableJpaRepositories("HaskellDefinitivisimo")
public class HaskellDefinitivisimoApplication {

	public static void main(String[] args) {
		SpringApplication.run(HaskellDefinitivisimoApplication.class, args);
	}

}
