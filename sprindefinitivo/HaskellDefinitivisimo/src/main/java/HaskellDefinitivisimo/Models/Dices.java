package HaskellDefinitivisimo.Models;

public class Dices {
	private int dices;
	private int idCorporation;
	public int getDices() {
		return dices;
	}
	public void setDices(int dices) {
		this.dices = dices;
	}
	public int getIdCorporation() {
		return idCorporation;
	}
	public void setIdCorporation(int idCorporation) {
		this.idCorporation = idCorporation;
	}
	public Dices(int dices, int idCorporation) {
		super();
		this.dices = dices;
		this.idCorporation = idCorporation;
	}
	public Dices(int dices) {
		super();
		this.dices = dices;
	}
	public Dices() {
		super();
	}
	@Override
	public String toString() {
		return "Dices [dices=" + dices + ", idCorporation=" + idCorporation + "]";
	}
	
	

}
