package HaskellDefinitivisimo.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import HaskellDefinitivisimo.Models.Makers;



public interface MakersRepository extends JpaRepository<Makers, Integer>{

}
