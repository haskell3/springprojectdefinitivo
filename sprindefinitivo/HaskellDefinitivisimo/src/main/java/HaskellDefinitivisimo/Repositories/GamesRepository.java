package HaskellDefinitivisimo.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import HaskellDefinitivisimo.Models.Games;


public interface GamesRepository extends JpaRepository<Games, Integer>{

}
