package HaskellDefinitivisimo.Repositories;


import org.springframework.data.jpa.repository.JpaRepository;

import HaskellDefinitivisimo.Models.Corporations;

public interface CorporationsRepository extends JpaRepository<Corporations, Integer>{
	

}
