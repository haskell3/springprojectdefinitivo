package HaskellDefinitivisimo.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import HaskellDefinitivisimo.Models.Games;
import HaskellDefinitivisimo.Repositories.GamesRepository;


@Service("GamesService")
public class GamesService {
	@Autowired
	GamesRepository gr;
	
	public void updateGame(Games g) {
		gr.save(g);
	}

}
