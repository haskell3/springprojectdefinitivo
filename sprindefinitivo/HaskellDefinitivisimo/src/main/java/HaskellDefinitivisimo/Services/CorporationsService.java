package HaskellDefinitivisimo.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import HaskellDefinitivisimo.Models.Corporations;
import HaskellDefinitivisimo.Repositories.CorporationsRepository;


@Service("CorporationsService")
public class CorporationsService {
	@Autowired
	CorporationsRepository cr;
	
	public List<Corporations> getAllCorporations() {
		return cr.findAll();
	}
	
	public void updateCorporation(Corporations c) {
		cr.save(c);
	}
	
	public Corporations findCorporationById(int id) {
		return cr.findById(id).orElse(null);
	}
	

}
