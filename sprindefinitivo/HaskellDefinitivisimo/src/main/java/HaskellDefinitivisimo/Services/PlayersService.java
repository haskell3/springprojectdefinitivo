package HaskellDefinitivisimo.Services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import HaskellDefinitivisimo.Models.Corporations;
import HaskellDefinitivisimo.Models.Players;
import HaskellDefinitivisimo.Repositories.PlayersRepository;

@Service("PlayersService")
public class PlayersService{
	@Autowired
	PlayersRepository pr;

	public List<Corporations> getCorporationsWithPlayer() {
		List<Players> players = pr.findAll();
		List<Corporations> corp = new ArrayList<Corporations>();
		for (Players player : players) {
			if (!corp.contains(player.getCorporationsPlayer())) {
				corp.add(player.getCorporationsPlayer());
			}
		}
		return corp;
		
	}
	
	public void updatePlayer(Players p) {
		pr.save(p);
	}

}
