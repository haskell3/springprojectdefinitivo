package HaskellDefinitivisimo.Services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import HaskellDefinitivisimo.Models.Makers;
import HaskellDefinitivisimo.Models.TypeMakers;
import HaskellDefinitivisimo.Repositories.MakersRepository;


@Service("MakersService")
public class MakersService {
	@Autowired
	MakersRepository mr;
	
	public List<Makers> getMakersByTypeWithoutCorporation (TypeMakers typeMakers) {
		List<Makers> mak = mr.findAll();
		List<Makers> makfinal = mr.findAll();
		
		for (Makers m : mak) {
			if (m.getCorporations() == null && m.getTypemaker() == typeMakers) {
				makfinal.add(m);
			}
		}
		return makfinal;
		
	}
	
	public List<Makers> getMakersByTypeWithCorporation (TypeMakers typeMakers, int idCorporation) {
		List<Makers> mak = mr.findAll();
		List<Makers> makfinal = mr.findAll();
		
		for (Makers m : mak) {
			if (m.getCorporations().getId() == idCorporation && m.getTypemaker() == typeMakers) {
				makfinal.add(m);
			}
		}
		return makfinal;
		
	}
	public List<Makers> getMakersByType(TypeMakers typemaker) {
		List<Makers> makers = mr.findAll();
		List<Makers> byType = new ArrayList<Makers>();
		for (Makers m : makers) {
			if (typemaker == m.getTypemaker()) {
				byType.add(m);
			}
		}
		return byType;
	}

	
	public boolean areOceansOccupied() {
		List<Makers> makers = getMakersByType(TypeMakers.oceà);
		for (Makers m : makers) {
			if (m.getCorporations() == null) {
				return false;
			}
		}
		return true;
	}
	
	public void updateMaker(Makers m) {
		mr.save(m);
	}


}
